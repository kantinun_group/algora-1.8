import java.util.Arrays;
import java.util.Scanner;

public class N1{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //System.out.println("Enter array size: ");
        int[] A = new int[sc.nextInt()];
        //System.out.println("Input numbers: ");
        for(int i=0; i<A.length; i++){
            A[i] = sc.nextInt();
        }

        ReverseArray(A);

        System.out.print("A = " + Arrays.toString(A));

    }

    public static void ReverseArray(int[] A) {
        for(int i=0; i<A.length/2; i++){
            int temp = A[i];
            A[i] = A[A.length-i-1];
            A[A.length-i-1] = temp;
        }

    }

}